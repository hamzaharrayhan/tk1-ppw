from django.urls import path

from . import views

app_name = 'djangu'

urlpatterns = [
    path('', views.index, name='index'),
]